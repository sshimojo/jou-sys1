.PHONY: build clean deploy

build:
	docker build --no-cache -t gcr.io/kubeegde-gcp/node-app:v1 node-app
	docker build --no-cache -t gcr.io/kubeegde-gcp/node-app:v2 node-app2
	gcloud docker -- push gcr.io/kubeegde-gcp/node-app:v1
	gcloud docker -- push gcr.io/kubeegde-gcp/node-app:v2

deploy:
	kubectl apply -f k8s/deployment.yaml
	kubectl apply -f k8s/deployment2.yaml
	kubectl apply -f k8s/service.yaml

clean:
	kubectl delete -f k8s/deployment.yaml
	kubectl delete -f k8s/deployment2.yaml
	kubectl delete -f k8s/service.yaml

